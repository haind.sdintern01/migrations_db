@extends('layouts.main')

@section('tittle')
    <title>ADD FORM</title>
@endsection

@section('link')
    @include('clients.blocks.links')
@endsection

@section('name_form')
    <h1>Add Form</h1>
@endsection

@section('menu')
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
        <li class="nav-item">
            <a href="{{route('index')}}" class="nav-link {{url()->current() == 'http://laravel.test' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List Form
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('add')}}" class="nav-link {{url()->current() == 'http://laravel.test/add' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Add Form
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('store')}}" class="nav-link {{url()->current() == 'http://laravel.test/update' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Update Form
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <div class="card-body">
        <form action="">
            <div class="card-body">
                <div class="form-group">
                    @include('clients.blocks.input', ['type' => 'text', 'name' => 'username'])
                    @include('clients.blocks.input', ['type' => 'password', 'name' => 'password'])
                    @include('clients.blocks.input', ['type' => 'password', 'name' => 'confirm_password'])
                    @include('clients.blocks.input', ['type' => 'text', 'name' => 'name'])
                    @include('clients.blocks.input', ['type' => 'text', 'name' => 'phone_number'])
                    @include('clients.blocks.input', ['type' => 'text', 'name' => 'age']) 
                </div>
            </div>                                           
            <div class="card-footer">
                @include('clients.blocks.button', ['name_button' => 'add new account'])
            </div>
        </form>
    </div>

@endsection

@section('js')
    @include('clients.blocks.js')    
@endsection