@extends('layouts.main')

@section('tittle')
    <title>LIST FORM</title>
@endsection

@section('link')
    @include('clients.blocks.links')
@endsection

@section('name_form')
    <h1>List Form</h1>
@endsection

@section('menu')
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
        <li class="nav-item">
            <a href="{{route('index')}}" class="nav-link {{url()->current() == 'http://laravel.test' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                List Form
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('add')}}" class="nav-link {{url()->current() == 'http://laravel.test/add' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Add Form
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('store')}}" class="nav-link {{url()->current() == 'http://laravel.test/update' ? 'active' : ''}}">
                <i class="nav-icon fas fa-copy"></i>
                Update Form
            </a>
        </li>
    </ul>
@endsection

@section('content')
    <h1>Đây là nơi hiển thị List</h1>
@endsection

@section('js')
    @include('clients.blocks.js')    
@endsection