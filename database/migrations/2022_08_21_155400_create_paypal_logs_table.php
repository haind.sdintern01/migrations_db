<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paypal_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('booking_id');
            $table->string('transaction_id');
            $table->string('sale_id');
            $table->string('amount');
            $table->text('transaction_log');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::table('paypal_logs', function(Blueprint $table)
        {
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paypal_logs');
    }
};
