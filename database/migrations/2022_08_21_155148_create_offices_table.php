<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('category_id');
            $table->string('company_name');
            $table->string('office_name');
            $table->string('map_url');
            $table->string('homepage_url');
            $table->string('manager_email');
            $table->string('tel');
            $table->mediumInteger('status');
            $table->string('start_work');
            $table->string('end_work');
            $table->string('description');
            $table->string('image');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
};
