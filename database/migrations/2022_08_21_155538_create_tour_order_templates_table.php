<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_order_templates', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->string('name');
            $table->unsignedBigInteger('price');
            $table->string('tourname_name');
            $table->text('tourname_explain');
            $table->string('tourname_image');
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept');
            $table->json('budget');
            $table->json('note');
            $table->text('contact_content');
            $table->string('contact_image');
            $table->string('public_url');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->text('tourname_image_preview');
            $table->text('tourname_image_thumbnail');
            $table->text('contact_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type')->default(1)->comment('1: Normal, 2: Furusato.');
            $table->bigInteger('furusato_tour_city_id')->unsigned();
            $table->string('furusato_code');
            $table->text('furusato_from_site');
            $table->json('extra_info');
            $table->tinyInteger('budget_remark_status')->default(4);
            $table->text('budget_remark');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_order_templates');
    }
};
