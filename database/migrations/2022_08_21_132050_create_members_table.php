<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->string('sei');
            $table->string('mei');
            $table->string('sei_kana');
            $table->string('mei_kana');
            $table->string('phone');
            $table->string('email');
            $table->date('birthday');
            $table->string('postal_code');
            $table->string('address');
            $table->unsignedTinyInteger('prefecture_id');
            $table->unsignedTinyInteger('city_id');
            $table->string('street');
            $table->string('building');
            $table->string('user_type');
            $table->unsignedTinyInteger('active');
            $table->unsignedTinyInteger('gender');
            $table->string('introducer_name');
            $table->timestamp('email_verified_at');
            $table->string('password');
            $table->string('tmp_password');
            $table->dateTime('last_login');
            $table->integer('login_fails');
            $table->dateTime('last_login_fail_at');
            $table->string('remember_token');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->dateTime('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
};
