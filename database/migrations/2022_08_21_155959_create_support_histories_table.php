<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('admin_id');
            $table->unsignedBigInteger('member_id');
            $table->datetime('datetime');
            $table->string('content');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::table('support_histories', function(Blueprint $table)
        {
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_histories');
    }
};
