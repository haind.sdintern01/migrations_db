<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_order_template_areas', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('tour_order_template_id');
            $table->unsignedBigInteger('prefecture_id');
            $table->unsignedBigInteger('city_id');
            $table->smallInteger('order_no');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });

        Schema::table('tour_order_template_areas', function(Blueprint $table)
        {
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->foreign('prefecture_id')->references('id')->on('prefectures')->onDelete('cascade');
            $table->foreign('tour_order_template_id')->references('id')->on('tour_order_templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour_order_template_areas');
    }
};
