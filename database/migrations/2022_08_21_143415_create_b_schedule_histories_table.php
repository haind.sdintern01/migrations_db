<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_schedule_histories', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('b_tour_order_id');
            $table->unsignedBigInteger('b_tour_order_schedule_id');
            $table->date('schedule_date');
            $table->unsignedTinyInteger('is_draft_reserved');
            $table->unsignedTinyInteger('is_final_reserved');
            $table->text('reserved_content');
            $table->unsignedBigInteger('admin_id');
            $table->string('admin_name');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->timestamp('deleted_at');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_schedule_histories');
    }
};
