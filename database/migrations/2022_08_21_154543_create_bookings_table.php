<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->unsignedBigInteger('id', true);
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('b_tour_order_id');
            $table->unsignedBigInteger('tour_order_template_id');
            $table->unsignedBigInteger('admin_id');
            $table->integer('discuss_method');
            $table->json('discuss_time');
            $table->date('hearing_first_time');
            $table->text('hearing_tour_purpose');
            $table->string('hearing_budget');
            $table->string('hearing_plan_time');
            $table->string('hearing_area');
            $table->unsignedBigInteger('adult_count');
            $table->unsignedBigInteger('child_count');
            $table->text('hearing_room_allocation');
            $table->text('hearing_meal_note');
            $table->text('hearing_tour_note');
            $table->text('hearing_other_note');
            $table->text('introduce_info');
            $table->date('start_date');
            $table->date('end_date');
            $table->date('application_deadline');
            $table->integer('proposal_status');
            $table->integer('invoice_status');
            $table->date('payment_deadline');
            $table->integer('payment_status');
            $table->integer('status');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            $table->timestamp('deleted_at');
            $table->date('payment_date');
            $table->unsignedBigInteger('payment_confirm_admin_id');
            $table->unsignedTinyInteger('is_receive_info');
            $table->unsignedTinyInteger('type');
            $table->text('note');
            $table->json('information_create_plan');
            $table->json('information_verify_plan');
            $table->json('information_draft_booking');
            $table->json('information_booking');
            $table->unsignedBigInteger('user_confirmed_id');
            $table->tinyInteger('first_time_support_review_status')->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('input_hearing_sheet_review_status')->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('input_tour_plan_review_status')->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('final_information_review_status')->comment('1: Reviewing, 2 finish_review');
            $table->tinyInteger('want_to_mail')->default(0);
            $table->datetime('lock_paypal_checkout');
            $table->unsignedBigInteger('adult_count_tour');
            $table->unsignedBigInteger('child_count_tour');
            $table->datetime('consultation_date');
            $table->tinyInteger('budget_remark_status')->default(1);
            $table->text('budget_remark');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('payment_confirm_admin_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
