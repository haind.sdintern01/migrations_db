<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\WelcomeController;


// Route::resources([
//     'users' => UserController::class,
//     'views' => ViewController::class,
//     'jobs' => JobController::class,
//     'welcomes' => WelcomeController::class
// ]);
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class, 'index'])->name('index');
Route::get('add', [UserController::class, 'create'])->name('add');
Route::get('update', [UserController::class, 'store'])->name('store');